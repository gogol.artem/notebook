import { Component } from '@angular/core';

export const broadCastChannel: BroadcastChannel = new BroadcastChannel('notesChange');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {

  }
}
