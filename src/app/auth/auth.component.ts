import { Component } from '@angular/core';
import { FormBuilder  } from '@angular/forms';
import { Validators } from '@angular/forms';
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {
  constructor(
    private fb: FormBuilder,
    private router: Router,
  ) {}

  authForm = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required]
  })

  get _login() {
    return this.authForm.get('login');
  }

  get _password() {
    return this.authForm.get('password');
  }

  onSubmit() {
    if (this.authForm.valid) {
      this.router.navigate(['notes'])
    }
  }
}
