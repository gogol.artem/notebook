import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { INote } from "./interfaces/note";

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const notes = [
      {
        id: 1,
        createDate: new Date("2011-10-12"),
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent venenatis libero nisi, vitae pellentesque ex ornare quis. Nunc rutrum sem nec lacinia commodo.'
      },
      {
        id: 2,
        createDate: new Date("2012-11-11"),
        text: 'Nulla vulputate, odio vitae vehicula luctus, sapien est aliquet sem, vitae porttitor quam turpis sed elit. Sed vel nisi a augue porta maximus quis aliquet libero.'
      },
      {
        id: 3,
        createDate: new Date("2013-12-13"),
        text: 'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec in dignissim arcu. '
      },
      {
        id: 4,
        createDate: new Date("2014-01-17"),
        text: 'Curabitur nunc tellus, rutrum non varius et, dignissim laoreet dui. Proin sem ex, pharetra id ex at, dictum cursus lectus. Phasellus a dictum libero.'
      },
      {
        id: 5,
        createDate: new Date("2015-03-14"),
        text: 'Vestibulum viverra malesuada felis fringilla ultricies. Mauris viverra magna nec diam egestas, ut consequat tortor tempus.'
      },
      {
        id: 6,
        createDate: new Date("2016-05-21"),
        text: 'Vestibulum blandit quam semper, elementum felis in, iaculis eros. In quis velit vestibulum, imperdiet velit vel, sodales ante.'
      },
      {
        id: 7,
        createDate: new Date("2017-07-31"),
        text: 'In cursus laoreet imperdiet. Phasellus at dictum ante, at varius nunc. Donec augue felis, fringilla et ante vel, porta iaculis lectus. Nulla tristique pellentesque mi ut gravida. Sed ac efficitur sapien, rutrum finibus erat.'
      },
    ]
    return {notes}
  }

  genId(notes: INote[]): number {
    return notes.length > 0 ? Math.max(...notes.map(note => note.id)) + 1 : 1;
  }
}
