import { INote } from "./interfaces/note";

export const NOTES: INote[] = [
  {
    id: 1,
    createDate: new Date("2011-10-12"),
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint! Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint!'
  },
  {
    id: 2,
    createDate: new Date("2012-11-11"),
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint!'
  },
  {
    id: 3,
    createDate: new Date("2013-12-13"),
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint! Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint!'
  },
  {
    id: 4,
    createDate: new Date("2014-01-17"),
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint!'
  },
  {
    id: 5,
    createDate: new Date("2015-03-14"),
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint!'
  },
  {
    id: 6,
    createDate: new Date("2016-05-21"),
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint!'
  },
  {
    id: 7,
    createDate: new Date("2017-07-31"),
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint! Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequatur cum cumque dolores est iusto libero nam quaerat rerum sint!'
  },
]
