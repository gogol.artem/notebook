import { Component, OnInit, ChangeDetectorRef  } from '@angular/core';
import { NoteService } from "../services/note.service";
import { INote } from "../interfaces/note";
import {broadCastChannel} from "../app.component";

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})

export class NotesComponent implements OnInit {
  notes: INote[] = []

  constructor(
    private noteService: NoteService,
    private changeDetection: ChangeDetectorRef
  ){
    broadCastChannel.onmessage = (e) => {
      this.notes = e.data;
      this.changeDetection.detectChanges();
    }
  }

  ngOnInit(): void {
    this.getNotes();
  }

  getNotes(): void {
    this.noteService.getNotes()
      .subscribe(notes => this.notes = notes)
  }

  add(text: string): void {
    text = text.trim();
    let createDate = new Date();
    this.noteService.addNote({ createDate, text } as INote)
      .subscribe(note => {
        this.notes.push(note);
        broadCastChannel.postMessage(this.notes);
      });
  }

  delete(note: INote): void {
    this.notes = this.notes.filter(n => n !== note);
    broadCastChannel.postMessage(this.notes);
    this.noteService.deleteNote(note.id).subscribe();
  }

  sortByDateAsc(): INote[] {
    return this.notes.sort((a: INote, b: INote) => <any>new Date(a.createDate) - <any>new Date(b.createDate))
  }

  sortByDateDesc(): INote[] {
    return this.notes.sort((a: INote, b: INote) => <any>new Date(b.createDate) - <any>new Date(a.createDate))
  }

  sortByNameAsc(): INote[] {
    return this.notes.sort((a: INote, b: INote) => a.text.localeCompare(b.text))
  }

  sortByNameDesc(): INote[] {
    return this.notes.sort((a: INote, b: INote) => b.text.localeCompare(a.text))
  }

}
