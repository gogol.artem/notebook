export interface INote {
  id: number,
  createDate: Date,
  text: string
}
