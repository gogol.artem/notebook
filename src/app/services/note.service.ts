import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { INote } from "../interfaces/note";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  private notesUrl = 'api/notes';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
  ) {}

  getNotes(): Observable<INote[]> {
    return this.http.get<INote[]>(this.notesUrl)
  }

  getNote(id: number): Observable<INote> {
    const url = `${this.notesUrl}/${id}`;
    return this.http.get<INote>(url)
  }

  updateNote(note: INote): Observable<any> {
    return this.http.put(this.notesUrl, note, this.httpOptions)
  }

  addNote(note: INote): Observable<INote> {
    return this.http.post<INote>(this.notesUrl, note, this.httpOptions)
  }

  deleteNote(id: number): Observable<INote> {
    const url = `${this.notesUrl}/${id}`;
    return this.http.delete<INote>(url, this.httpOptions)
  }
}
